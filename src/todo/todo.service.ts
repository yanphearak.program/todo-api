import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Todo } from './todo.entity';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(Todo)
    private readonly TodoRepository: Repository<Todo>,
  ) {}

  async findAll(): Promise<Todo[]> {
    return await this.TodoRepository.find();
  }

  async findOne(id: any): Promise<Todo> {
    return await this.TodoRepository.findOne({where: {id}});
  }

  async create(todo: Todo): Promise<Todo> {
    return await this.TodoRepository.save(todo);
  }

  async update(id: number, todo: Todo): Promise<boolean> {
    const result = await this.TodoRepository.update(id, todo);
    return result.affected > 0;
  }

  async delete(id: number): Promise<boolean> {
    const result = await this.TodoRepository.delete(id);
    return result.affected > 0;
  }
}